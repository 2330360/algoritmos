Proceso Videos_Repetitivas
	Definir candidato1, candidato2, candidato3, cont, cantidad, voto Como Entero;
	
	candidato1 <- 0;
	candidato2 <- 0;
	candidato3 <- 0;
	cont <- 0;
	
	Escribir "Los candidatos son: ";
	Escribir "1.- Luis Gonzales";
	Escribir "2.- Martin Ibarra";
	Escribir "3.-Andrea Salazar";
	
	Escribir "Cantidad de votos que contara";
	leer cantidad;
	
	Repetir
		Escribir "Ingrese candidato que apoya";
		leer voto;
		Segun voto hacer
			1:
				candidato1 <- candidato1 +1;
			2:
				candidato2 <- candidato2 +1;
			3:
				candidato3 <- candidato3 +1;
			De Otro Modo:
				Escribir "No es un candidato";
		FinSegun
		Si voto == 1 || voto == 2 || voto == 3 Entonces
			cont <- cont + 1;
		FinSi
		
	Hasta Que cont == cantidad
	
	Escribir "La cantidad de votos por candidato es: ";
	Escribir "Luis gonzales ", candidato1;
	Escribir "Martin Ibarra ", candidato2;
	Escribir "Andrea Salazar: ", candidato3;
FinProceso
