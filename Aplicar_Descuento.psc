Algoritmo Aplicar_Descuento
	Definir N, X, R Como Real;
	Escribir "Ingrese una cantidad";
	Leer N;
	Escribir "Que descuento deseas aplicar?";
	Escribir "1)10%";
	Escribir "2)15%";
	Escribir "3)20%";
	Leer X;
	Segun X Hacer
		1:
			R <- N-(N* .10);
			Escribir "El descuento aplicado es: ", R;
		2:
			R <- N-(N* .15);
			Escribir "El descuento aplicado es: ", R;
		3:
			R <- N-(N* .20);
			Escribir "El descuento aplicado es: ", R;
		De Otro Modo:
			Escribir "Ingrese un descuento valido";
	FinSegun
FinAlgoritmo
