Algoritmo Calcular_el_importe_total_de_la_venta
	Definir M, IMPA,C, DLP, T Como real;
	Escribir "Bienvenido al aereopuerto a donde desea viajar?";
	Escribir "Seleccione el Estado a donde quisiera viajar";
	Escribir "(1)Distrito Federal, (2)Guadalajara, (3)Monterrey, (4)Cancun, (5)Tijuana.";
	Leer IMPA;
	Segun IMPA Hacer
		1: IMPA <- 450;
			
		2: IMPA <- 500;
			
		3: IMPA <- 600;
			
		4: IMPA <- 750;
			
		5: IMPA <- 350;
			
		De Otro Modo:
			Escribir "Disculpe, eligio una opcion erronea";
			
	FinSegun
	Escribir "Cuantas millas usted va a recorrer";
	Leer M;
	Si M >= 1 y M <= 1000 Entonces
		M <- M*10;
	SiNo
		Si M >= 1000 Y M <= 2000 Entonces
			M <- M * 7
		FinSi
		Si M > 2000 Entonces
			M <- M * 5
		FinSi
	FinSi
	Escribir "Cual seria la clase de su viaje?";
	Escribir "(1) Turista, (2) Ejecutiva, (3) Primera clase";
	Leer C;
	Segun C Hacer
		1:
			C <- M + 0;
		2: 
			C <- M*1.10;
		3:
			C <- M*1.20
		De Otro Modo:
			Escribir "Disculpe, introdujo una opcion erronea";
	FinSegun
	
	T<- C + IMPA;
	
	Escribir "Cual es la edad del pasajero?";
	Leer DLP
	Si DLP >= 60 Entonces
		T <- T * 0.5;
	FinSi
	T1<- T * 1.16
	Escribir "Impuesto aereo:" IMPA;
	Escribir "Precio del viaje:" M;
	Si DLP >= 60 Entonces
		Escribir "Descuento del adulto mayor: -" T;
	FinSi
	Escribir "IVA: " T1 - T;
	Escribir "Total a pagar: " T1;
	
	
FinAlgoritmo
