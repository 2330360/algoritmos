Algoritmo Elegir
	Definir Tall Como Entero
	Escribir "Elija el numero correspondiente a la talla de su ropa";
	Escribir "(1) Chica";
	Escribir "(2) Mediana";
	Escribir "(3) Grande";
	Escribir "(4) Extra grande";
	Leer Tall;
	Segun Tall Hacer
		1:
			Escribir "Eligio la talla chica, puede tomar su ropa";
		2:
			Escribir "Eligio la talla mediana, puede tomar su ropa";
		3:
			Escribir "Eligio la talla grande, puede tomar su ropa";
		4:
			Escribir "Eligio la talla extra grande, puede tomar su ropa";
		De Otro Modo:
			Escribir "No tenemos ropa en existencia de su talla, vuelva pronto";
	FinSegun
FinAlgoritmo


