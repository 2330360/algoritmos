Algoritmo sin_titulo
	Definir a,b,c,d,e Como Entero;
	a<-6;
	b<-10;
	c<-12;
	Escribir "OPERADORES AlGEBRAICOS";
	Escribir "1: (a + 6) * (a - 10) % (a ^ 15) + (a % 4) - (a / 2) ^ (a * 6) = ",(a + 6) * (a - 10) % (a ^ 15) + (a % 4) - (a / 2) ^ (a * 6);
	Escribir "2: (b + 4) * (b - 2) % (b ^ 9) - (b % 2) + (b / 7) ^ (b * 8) = ",(b + 4) * (b - 2) % (b ^ 9) - (b % 2) + (b / 7) ^ (b * 8);
	Escribir "3: (c + 12) * (c - 10) % (c ^ 1) - (c % 7) + (c / 3) ^ (c * 2) = ",(c + 12) * (c - 10) % (c ^ 1) - (c % 7) + (c / 3) ^ (c * 2);
	
	Escribir "OPERADORES LOGICOS";
	Escribir "1: (a < 3) & (a = 20 ) o (a = 5  Y 2 = 1) o No (4<5) = ",(a < 3) & (a = 20 ) o (a = 5  Y 2 = 1) o No (4<5); 
	Escribir "2: (b < 4) & (b = 10 ) o (b = 10  Y 2 = 2) o NO(5<4) = ",(b < 4) & (b = 10 ) o (b = 10  Y 2 = 2) o NO(5<4); 
	Escribir "3: (c < 14) & (c = 12 ) o (c = 12  Y 6 = 6) o NO (4<3) = ",(c < 14) & (c = 12 ) o (c = 12  Y 6 = 6) o NO (4<3);
	
	Escribir "OPERADORES RELACIONALES";
	Escribir "1: (a < 4) = (a > 5 ) = (a <= 23 ) <> (a = 6 ) = (a >= 12 ) = ",(a < 4) = (a > 5 ) = (a <= 23 ) <> (a = 6 ) = (a >= 12 );
	Escribir "2: (b < 8) = (b > 12 ) = (b <= 16 ) <> (b = 10 ) = (b >= 19 ) = ",(b < 8) = (b > 12 ) = (b <= 16 ) <> (b = 10 ) = (b >= 19 );
	Escribir "3: (c < 10) = (c > 15 ) = (c <= 13 ) <> (c = 12 ) = (c >= 16 ) = ",(c < 10) = (c > 15 ) = (c <= 13 ) <> (c = 12 ) = (c >= 16 );
FinAlgoritmo
