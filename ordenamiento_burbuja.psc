Algoritmo ordenamiento_burbuja
	//definir variables
	Definir a, i, j, aux Como Entero;
	Dimension a[3];
	a[1]=10
	a[2]=4
	a[3]=2
	//mostrar el arreglo
	para i=1 hasta 3 con paso	1 Hacer
		Escribir sin saltar a[i]
	FinPara
	Escribir "Vector original"
	//procedimiento en ordenamiento burbuja
	Para i=2 hasta 3 Con Paso 1 Hacer
		Para j=1 hasta 2 con paso 1 Hacer
			si a[j] > a[j+1] Entonces
				aux=a[j]
				a[j]=a[j+1]
				a[j+1]=aux
			SiNo
			FinSi
		FinPara
	FinPara
	//imprimir el vector ordenado 
	para i=1 hasta 3 con paso 1 Hacer
		Escribir sin saltar a[i]
		Escribir "vector ordenado: "
	FinPara
	FinAlgoritmo
	