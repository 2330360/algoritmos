Algoritmo Deportes_extremos
	Definir a,b,c Como Caracter;
	Escribir "Tiene alguna lesion?"
	Leer a;
	Escribir "Tiene alguna enfermedad ya sea mortal o de alto riesgo?";
	Leer b;
	
	Si a="no"&b="no" Entonces
		Escribir "No se diga mas, por ahora estos deportes estan disponibles";
		Escribir "Paracaidismo, Bunque, Motocros, Alpinismo, Parapente, Surf y Salto base";
		Escribir "Alguno te llama la atencion?";
		Leer c;
		Si c="si" Entonces
			Escribir "Entonces vamos a hablar un poco mas sobre el deporte que te interesa";
		SiNo
			Escribir "Que lastoma son los unicos que tenemos disponibles";
		FinSi
	SiNo
		Si a="si"&b="si" Entonces
			Escribir "Disculpame pero no es posible que practique un deporte extremo";
		Sino
			Si a="si" Entonces
				Escribir "No puede practicar cualquier tipo de deporte extremo si tiene una lesion";
			Sino
				Escribir "No es posible que pracique un deporte extremo con alguna enfermedad que pondria en riesgo su vida";
			FinSi
		FinSi
		
	FinSi
FinAlgoritmo
