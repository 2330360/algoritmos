Algoritmo Opciones_de_juegos
	Definir juego Como Real
	Escribir 'Bienvenido a la busqueda de juegos'
	Escribir 'Seleccione el juego que desea obtener'
	Escribir '(1)Gta'
	Escribir '(2)Sonic'
	Escribir '(3)Dragon ball'
	Escribir '(4)Batman'
	Escribir '(5)Mario'
	Leer juego
	Seg�n juego Hacer
		1:
			juego <- 1
		2:
			juego <- 2
		3:
			juego <- 3
		4:
			juego <- 4
		5:
			juego <- 5
		De Otro Modo:
			Escribir 'Disculpe, eligio una opcion erronea'
	FinSeg�n
	Si juego=1 Entonces
		Escribir '(Puede obtener juegos como)'
		Escribir '1.GTA'
		Escribir '2.Grand Theft Auto III'
		Escribir '3.Grand Theft Auto: Vice City'
		Escribir '4.Grand Theft Auto V'
	SiNo
		Si juego=2 Entonces
			Escribir '(Puede obtener juegos como)'
			Escribir '1.Sonic Generations (PC, Xbox 360, PS3 - 2011)'
			Escribir '2.Sonic Advance 2 (GBA - 2002)'
			Escribir '3.Sonic Rush (Nintendo DS - 2005)'
			Escribir '4.Sonic Colours (Wii - 2010)'
			Escribir '5.Sonic the Hedgehog (Mega Drive - 1991)'
		SiNo
			Si juego=3 Entonces
				Escribir '(Puede obener juegos como)'
				Escribir '1.Dragon Ball Z: Budokai Tenkaichi 3.'
				Escribir '2.Dragon Ball Z: Kakarot'
				Escribir '3.Dragon Ball Z: Budokai 3'
				Escribir '4.Dragon Ball Advance Adventure'
				Escribir '5.Dragon Ball: Origins'
			SiNo
				Si juego=4 Entonces
					Escribir '(Puede obtener juegos como)'
					Escribir '1.LEGO Batman 2: DC Super Heroes'
					Escribir '2.Batman Returns (MD / Sega CD)'
					Escribir '3.Batman: Arkham Origins'
					Escribir '4.Batman Returns (SNES)'
					Escribir '5.Batman: Arkham Knight'
				SiNo
					Si juego=5 Entonces
						Escribir '(Puede obtener juegos como)'
						Escribir '1.Super Mario 3D Land (2011)'
						Escribir '2.Super Mario Bros. 2 (1988)'
						Escribir '3.Super Mario 3D Land (2011)'
						Escribir '4.Super Mario Galaxy 2'
						Escribir '5.Super Mario 64'
					SiNo
						Escribir 'No elegiste una opcion valida'
					FinSi
				FinSi
			FinSi
		FinSi
	FinSi
FinAlgoritmo
