Algoritmo Condicional
	Definir metros Como Real;
	Definir desicion Como Entero;
	
	Escribir "Ingrese la cantidad de metros";
	Leer metros;
	Escribir "Escriba | 1 Pies | 2 Pulgadas | 3 Centimetros | 4 Kilometros | 5 yardas | ";
	Leer desicion;
	
	Segun decision Hacer
		1:
			Escribir metros, "metros en pies =", metros*3.28084;
		2:
			Escribir metros, "metros en pulgadas =", metros*39.3701;
		3:
			Escribir metros, "metros en centimetros =", metros*100;
		4:
			Escribir metros, "metros en kilometros =", metros/100;
		5:
			Escribir metros, "metros en yardas =", metros*1.094;
		De Otro Modo:
			Escribir "No esta dentro del rango establecido";
	FinSegun
FinAlgoritmo
